const express = require(`express`);
const router = express.Router();
const userController = require(`../controllers/user`)

// Route checking if the user's email already exists in  the ddatabase

// endpoint: localhost:4000/users/checkEmail
router.post(`/checkEmail`, (req,res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
}) 

// Route for registering a user
router.post('/register', (req,res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication
router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})
// router to get all profile
router.get("/", (req,res) => {
    userController.getAllUsers().then(resultFromController => res.send(resultFromController));
})

// router to get user profile
router.get(`/details`, (req,res) => {
    userController.getProfile(req.body.id).then(resultFromController => res.send(resultFromController));
})



module.exports =  router;