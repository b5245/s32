// Import the User model
const req = require("express/lib/request");
const User = require(`../models/User`);
const bcrypt = require(`bcrypt`);
const auth = require(`../auth`);

// Controller function for checking email  duplicates
module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0){
            return result;
        }
        else {
            return false;
        }
    })
}

// Controller function for user registration
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {
        if(error){
            // user registration failed
            console.log(error);
            return false;
        }
        // Registration successful
        else{
            return true;
        }
    })
}

// user authentication (/login)
module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody}).then(result => {
        // User does not exist
        if(result == null){
            return false
        }
        // User exists
        else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect == true){
                // Generate an access token
                return { access : auth.createAccessToken(result)}
            }
            // password do not match
            else{
                return false;
            }
        }
    })
}

// controller to get all users
module.exports.getAllUsers = () => {
    return  User.find ({}).then(result => {
        return result;
    })
}

// controller to get a profile
module.exports.getProfile = (userId) => {
    return User.findById(userId).then((result) => {
        result.password = ``
        return result;
    })
}