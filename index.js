const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our frontend application
const cors = require("cors");

// Allows access to routes defined within the application
const userRoutes = require(`./Routes/user`)

const app = express();

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.mgfy3.mongodb.net/s32-s36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Propmts a message for successful database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

// Allows all resources to access the backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the `/users` string to be included for all user routes defined in the "user" route file
app.use(`/users`, userRoutes)

// App listening to port 4000
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});
