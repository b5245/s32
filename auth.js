const jwt = require("jsonwebtoken");

// User defined string data that will be used to create JSON web tokens
const secret = "CourseBookingAPI"

// Token Creation
module.exports.createAccessToken = (user) => {
    const data = {
        id : user._id,
        email : user.email,
        isAdmin: user.isAdmin
    };
                                // {} states that no aditional option will be added
    return jwt.sign(data, secret, {})
}
